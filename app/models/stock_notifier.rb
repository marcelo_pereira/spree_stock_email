require 'sendwithus_ruby_action_mailer'
class StockNotifier < ::SendWithUsMailer::Base

  default from: 'atendimento@dragonflyvc.com'

  def order_stocked(recipient, orders)

      assign :product, {name: orders}

      mail(
          email_id: 'tem_sXehtjXLfBEHBUCDv27MXd',
          recipient_address: recipient,
      )
  end
end
