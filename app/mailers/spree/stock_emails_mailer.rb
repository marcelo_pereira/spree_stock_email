require 'sendwithus_ruby_action_mailer'

module Spree
  class StockEmailsMailer < BaseMailer
    def stock_email(stock_email)
      @stock_email = stock_email

      mail = StockNotifier.order_stocked(@stock_email.email, @stock_email.product.name)
      mail.deliver

    end
  end
end
